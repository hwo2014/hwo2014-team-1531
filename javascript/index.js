var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var pedal,beta,lapDistance,previousDistance,turboCheck,previousSpeed,isBetaInitialized,conditions,lanes,sendSwitchLaneMsg;
var isQualifyingRace,numberOfLaps;
var startTime,currentLap,currentTick;
var isTurboAvaliable,sendTurboPerPieceMsg,isDuringTurbo,turboTickCount,turboDurationTicks,isEnableTurboStrategy,turboFactor;
var totalTick,velocitySum;

function Condition(isBend,isSwitch,l,r,angle,v,turbo,switchLane){
    this.isBend = isBend;
    this.isSwitch = isSwitch;
    this.l = l;
    this.r = r;
    this.angle = angle;
    this.turbo = turbo;
    this.switchLane = switchLane;
}

function Lane(index,distance){
    this.index = index;
    this.distance = distance;
}


function RaceEndStatisticPrint(){
	var currentdate = new Date(); 	
    var ServerEndedTime="Server start@ " + startTime.getDate() + "/"
                + (startTime.getMonth()+1)  + "/" 
                + startTime.getFullYear() + " @ "  
                + startTime.getHours() + ":"  
                + startTime.getMinutes() + ":" 
                + startTime.getSeconds();
    console.log(ServerEndedTime);     
    var averageVelocity= velocitySum/totalTick;
    console.log('Average velocity: ',averageVelocity);    
}

function GetTurboTick(){
  	return parseInt(turboDurationTicks*3/4);
}

function GetDriftingVelocity(acc,angle,v){
    return Math.sqrt((2*(acc-(beta*Math.sin(angle)))) + v*v);
}

function InitializeGlobalVariables()
{
    pedal = 0; 
    beta = 0.4;
    lapDistance = 0;
    previousDistance = 0;
    turboCheck = 8;
    previousSpeed = 0;
    isBetaInitialized = false;
    accSum = 0;
    conditions = new Array();
    lanes = new Array();
    sendSwitchLaneMsg = new Array();

    //For race session
    isQualifyingRace=false;
    numberOfLaps=0;

    //For printing server start time purpose
    startTime=new Date();
    currentLap=-1;
    currentTick=0;

    //For turbo
    isTurboAvaliable=false;
    sendTurboPerPieceMsg = new Array();
    isDuringTurbo=false;
    turboTickCount=0;
    turboDurationTicks=0;
    isEnableTurboStrategy=false;
    turboFactor=0;

    //Calculate average velocity
    totalTick=0;
    velocitySum=0;
}

function SwitchLaneAlgorithm(){
        for(var i = 0; i<conditions.length;i++)
        {
            if(conditions[i].isSwitch === true)
            {
                var a = i+1;
                while(conditions[a%conditions.length].isBend!=true) a++;

                var b = a;
                while(conditions[a%conditions.length].angle === conditions[b%conditions.length].angle) b++;

                var angle;
                if(conditions[b%conditions.length].angle != undefined) 
                {
                    var c = b;
                    while(conditions[b%conditions.length].angle === conditions[c%conditions.length].angle) c++;

                    if((c-b) < (b-a)) angle = conditions[a%conditions.length].angle;
                    else angle = conditions[b%conditions.length].angle;
                }
                else 
                {
                    angle = conditions[a%conditions.length].angle;
                }


                if(angle > 0) conditions[(i+conditions.length-1)%conditions.length].switchLane = "Right";
                else conditions[(i+conditions.length-1)%conditions.length].switchLane = "Left";
                console.log("i:",(i+conditions.length-1)%conditions.length,"angle:", angle);
            }  
        }
}

function ApplySimpleTurboStrategy(v,pieceIndex){
    if(isTurboAvaliable === true && conditions[pieceIndex].turbo === true )
    {
    	send({
    	msgType: "turbo", 
    	data: "Pow pow pow pow pow, Pow pow pow pow pow!"
    		});
        console.log("Start turbo in next 30 tickers!");
        isTurboAvaliable = false;
        isDuringTurbo=true;
    }

    if(isDuringTurbo === true)
    {
        if(turboTickCount === turboDurationTicks)
        {
            turboTickCount=0;
            isDuringTurbo = false;
        }
        else if(turboTickCount < turboDurationTicks)
        {
            var p = 0;
            if(isQualifyingRace === true)
            {
                p = 1;
                console.log("***********I believe I can fly**************！")
            }
            else
            {
                var index = pieceIndex+1;
                while(conditions[(index)%conditions.length].isBend != true) index ++;

                if(v>conditions[(index)%conditions.length].getV()) p = 1/turboFactor;
                else 
                {
                    isDuringTurbo = false;
                    p = 1;
                }
                console.log(".............",conditions[(index)%conditions.length].getV(), "v:",v,"p:",p);
            }
            turboTickCount++
		    send({
     		msgType: "throttle",
		    data: p
		    });
        }
    }
}

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });

    /*return send({
    "msgType": "createRace", "data": {
        "botId": {
        "name": botName,
        "key": botKey
        },
    "trackName": "england",
    "password": "",
    "carCount": 1
    }});  */
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
    if (data.msgType === 'carPositions') {       
        for (var i=0;i<data.data.length; i++)
        {
            if(data.data[i].id.name === botName)
            {
                var carPosition = data.data[i];
                break;
            }
        }
        var lap = carPosition.piecePosition.lap;
        var pieceIndex = carPosition.piecePosition.pieceIndex;
        var laneIndex = carPosition.piecePosition.lane.startLaneIndex;
        var inPieceDistance = carPosition.piecePosition.inPieceDistance;
        var driftAngle = carPosition.angle;

        if(conditions[pieceIndex].switchLane != undefined && sendSwitchLaneMsg[pieceIndex] === true)
        {
            send({"msgType": "switchLane", "data": conditions[pieceIndex].switchLane});
            sendSwitchLaneMsg[pieceIndex] = false;
            console.log("switch lane!!!!");
        }

        // calc current velocity
        var pieceLength = 0;
        for(var i=0; i<pieceIndex; i++) 
        {
            pieceLength += conditions[i].l;
        }

        var lapLength = 0;
        for(var i = 0; i< lap; i++)
        {
            for(j = 0; j< conditions.length; j++)
            {
                lapLength += conditions[i].l;
            }
        }

        pieceLength+=lapLength;
        var currentDistance = pieceLength + inPieceDistance;
        var v = currentDistance - previousDistance;
        previousDistance = currentDistance;  
        var acc = v - previousSpeed;
        previousSpeed = v;

        if(acc > 0 && acc < 1 && isBetaInitialized === false)
        {
            beta=acc*2.05;
            isBetaInitialized = true
            console.log("beta -->",beta);
        }

        if(GetDriftingVelocity(acc,driftAngle,v)>conditions[(pieceIndex+1)%conditions.length].getV()) pedal = 0;
        else pedal = 1;

        ApplySimpleTurboStrategy(GetDriftingVelocity(acc,driftAngle,v),pieceIndex);

     	send({
   		 	msgType: "throttle",
    		data: pedal
    		});

        console.log('Lap:',lap,'Piece Index:',pieceIndex,'Velocity:',v,'Accelerator:',acc,'Pedal:',pedal,"PieceCondition:",conditions[pieceIndex].isBend ? "bend" : "straig");
        
        velocitySum+=v;
        ++totalTick;       
    } 
    else if (data.msgType === 'gameInit') {
        InitializeGlobalVariables();

        var laneInfo = data.data.race.track.lanes;
        var minDistance = 0;
        for (var i=0; i<laneInfo.length;i++)
        {
            lanes[i] = new Lane(laneInfo[i].index,laneInfo[i].distanceFromCenter);
            if(lanes[i].distance < minDistance) minDistance = lanes[i].distance;
        }

        var pieces = data.data.race.track.pieces;
        for (var i=0; i<pieces.length;i++)
        {
            conditions[i] = new Condition(
                                pieces[i].length === undefined,
                                pieces[i].switch, 
                                pieces[i].length != undefined ? pieces[i].length : (Math.abs(pieces[i].angle)*Math.PI*(pieces[i].radius+minDistance)/180), 
                                pieces[i].radius, 
                                pieces[i].angle,
                                false, undefined);
            conditions[i].getV = function(){return this.r != undefined? Math.sqrt(beta*this.r) : 99}
            sendSwitchLaneMsg[i] = true;
            sendTurboPerPieceMsg[i]=true;
        }

        for(var i = 0; i<conditions.length;i++)
        {
            var j = i;
            while(conditions[j%conditions.length].isBend!=true) j++;
            if(j-i>=turboCheck) conditions[i].turbo = true;
        }

        if(isEnableTurboStrategy===true){        
            if(data.data.race.raceSession.quickRace===undefined||data.data.race.raceSession.quickRace===true){
            	isQualifyingRace=true;
            }else{
            	isQualifyingRace=false;
            }

            console.log(isQualifyingRace ? "----------------->It's in qualifying race!" : "----------------->It's in real race!");        
        }
        
        numberOfLaps=data.data.race.raceSession.laps;
                
        SwitchLaneAlgorithm();

        for(var i = 0; i<conditions.length;i++) console.log(i, conditions[i].l, conditions[i].r, conditions[i].angle, conditions[i].switchLane, conditions[i].isSwitch,conditions[i].getV() );
    }   
    else if (data.msgType === 'lapFinished') {
        for (var i=0; i<conditions.length;i++)
        {
            sendSwitchLaneMsg[i] = true;
            sendTurboPerPieceMsg[i]=true;
        }
        console.log('lap '+ data.data.lapTime.lap+' finished.')
    } 
    else if(data.msgType==='turboAvailable'){
    	isTurboAvaliable=true;
    	turboDurationTicks=data.data.turboDurationTicks;
        turboFactor = data.data.turboFactor;
    	console.log("Turbo is ready. Go Go Go!");
    }
    else if(data.msgType==='crash'){
        if(data.data.name===botName) 
        {
            beta-=0.1;
            console.log('slow beta to',beta);
        }     
    }
    else {
        if (data.msgType === 'join') {
            console.log('Joined');
        } else if (data.msgType === 'gameStart') {
        	send({
       		 	msgType: "throttle",
        		data: 1
        		});
        	console.log('Race started');
            startTime=new Date();
        } else if (data.msgType === 'gameEnd') {        	
            RaceEndStatisticPrint();
            console.log('Race ended');
        } else if(data.msgType==='dnf'){
        	console.log('Disqualified!');
        } else if(data.msgType==='spawn'){
        	console.log('Spawned and go on running.')
        }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
